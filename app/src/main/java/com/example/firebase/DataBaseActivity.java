package com.example.firebase;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

public class DataBaseActivity extends AppCompatActivity {

    EditText edt_title,  edt_body;


    Button btn_grabar, btn_listar_todos, btn_actualizar, btn_eliminar;

    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_base);

        edt_body = findViewById(R.id.edt_body);
        edt_title = findViewById(R.id.edt_title);
        btn_grabar = findViewById(R.id.btn_grabar);
        btn_listar_todos = findViewById(R.id.btn_listar_todos);
        btn_actualizar = findViewById(R.id.btn_actualizar);
        btn_eliminar = findViewById(R.id.btn_eliminar);


        firebaseDatabase = firebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("DBPost");

        btn_grabar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String title = edt_title.getText().toString();
                String body = edt_body.getText().toString();

                Post post = new Post();
                post.setTitle(title);
                post.setBody(body);


                //push crea la llave unica
                databaseReference.child("Post").push().setValue(post).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(DataBaseActivity.this, "OK", Toast.LENGTH_SHORT).show();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(DataBaseActivity.this, "Error", Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });

        btn_listar_todos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                databaseReference.child("Post").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        //cada vez que cambie algo en la database este evento se va desencadenar

                            for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                                Post post = snapshot.getValue(Post.class);

                                String title  = post.getTitle();
                                String body = post.getBody();
                                String id = snapshot.getKey();
                                Log.d("edinsonhq",body);
                                Log.d("edinsonhq", post.getTitle());
                            }




                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        });

        btn_actualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Map<String,Object> actualizacion = new HashMap<>();
                actualizacion.put("/title", "Actualizacion title");
                actualizacion.put("/body", "Actualizacion body");

                databaseReference.child("Post").child("LfXV6AodumTZxorPzC5").updateChildren(actualizacion);
                Toast.makeText(DataBaseActivity.this, "Actualizado", Toast.LENGTH_SHORT).show();

            }
        });

        btn_eliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                databaseReference.child("Post").child("-LfXV64MZMOgOszcq8-S").removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(DataBaseActivity.this, "eliminado", Toast.LENGTH_SHORT).show();

                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(DataBaseActivity.this, "error", Toast.LENGTH_SHORT).show();

                    }
                });
            }
        });
    }
}
