package com.example.firebase;

import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

public class RemoteConfigActivity extends AppCompatActivity {

    TextView tv_version;

    FirebaseRemoteConfig firebaseRemoteConfig;

    private static final int VERSION = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remote_config);

        firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        tv_version =   findViewById(R.id.tv_version);

        long cacheExpiration = 0;

        firebaseRemoteConfig.fetch(cacheExpiration)
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful()){
                            Toast.makeText(RemoteConfigActivity.this, "exitoso", Toast.LENGTH_SHORT).show();
                            firebaseRemoteConfig.activateFetched();
                            displayDialog();
                        }else{
                            Toast.makeText(RemoteConfigActivity.this, "", Toast.LENGTH_SHORT).show();
                        }
                    }
                });


    }

    private void displayDialog() {
        String version = firebaseRemoteConfig.getString("version");
        tv_version.setText(version);

        createSimpleDialog().show();
    }

    private AlertDialog createSimpleDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(RemoteConfigActivity.this);

        builder.setTitle("titulo")
                .setMessage("hay una nueva version, desea actualizar?")
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(RemoteConfigActivity.this, "OK", Toast.LENGTH_SHORT).show();
                    }
                })
                .setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(RemoteConfigActivity.this, "NO", Toast.LENGTH_SHORT).show();
                    }
                });

        return builder.create();
    }
}
